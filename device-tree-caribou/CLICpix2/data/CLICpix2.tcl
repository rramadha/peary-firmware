#
# (C) Copyright 2018 Adrian Fiergolski
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#

proc generate {drv_handle} {
	# try to source the common tcl procs
	# assuming the order of return is based on repo priority
	foreach i [get_sw_cores device_tree] {
		set common_tcl_file "[get_property "REPOSITORY" $i]/data/common_proc.tcl"
		if {[file exists $common_tcl_file]} {
			source $common_tcl_file
			break
		}
	}

	regsub "CLICpix2_(\d*)" $drv_handle {} index
        add_receiver $drv_handle $index
	add_control $drv_handle	$index
	add_spi $drv_handle $index

}

proc add_receiver {drv_handle index} {

    set i 0
    set ip_mem_handle [lindex [hsi::utils::get_ip_mem_ranges [get_cells -hier $drv_handle]] $i]
    while {[get_property ADDRESS_BLOCK $ip_mem_handle] != "Reg0"} {
	set ip_mem_handle [lindex [hsi::utils::get_ip_mem_ranges [get_cells -hier $drv_handle]] $i]
	incr i
    }
    if { [string_is_empty $ip_mem_handle] } {
	dtg_warning "ERROR: Can't find CLICpix2 receiver base address"
	return -1
    }
    set baseAddress [string tolower [get_property BASE_VALUE $ip_mem_handle]]
    set ReceiverNode [add_or_get_dt_node -l "CLICpix2_receiver$index" -n "CLICpix2_receiver" -u [regsub -all {^0x} $baseAddress {}] -p [gen_peripheral_nodes $drv_handle]]

    hsi::utils::add_new_dts_param $ReceiverNode "compatible" "xlnx,CLICpix2-receiver-1.0" stringlist
    #hsi::utils::add_new_dts_param $ReceiverNode "interrupt_parent" [hsi::utils::get_interrupt_parent $drv_handle "receiver_irpt"] reference
    set intr_info [get_intr_id $drv_handle "receiver_irpt"]
    if { [llength $intr_info] && ![string match -nocase $intr_info "-1"] } {
	hsi::utils::add_new_dts_param $ReceiverNode "interrupts" $intr_info intlist
    } else {
	dtg_warning "ERROR: ${drv_handle}: receiver_irpt port is not connected"
    }

    set regs [split [get_property CONFIG.reg $drv_handle]]
    set regs_index [lsearch $regs $baseAddress]
    set reg_value [concat [lindex $regs $regs_index] [lindex $regs [expr $regs_index + 1]]]
    hsi::utils::add_new_dts_param $ReceiverNode "reg" $reg_value inthexlist    
}

proc add_control {drv_handle index} {
    set i 0
    set ip_mem_handle [lindex [hsi::utils::get_ip_mem_ranges [get_cells -hier $drv_handle]] $i]
    while {[get_property ADDRESS_BLOCK $ip_mem_handle] != "Reg1"} {
	set ip_mem_handle [lindex [hsi::utils::get_ip_mem_ranges [get_cells -hier $drv_handle]] $i]
	incr i
    }
    if { [string_is_empty $ip_mem_handle] } {
	dtg_warning "ERROR: Can't find CLICpix2 receiver base address"
	return -1
    }

    set baseAddress [string tolower [get_property BASE_VALUE $ip_mem_handle]]
    set ControlNode [add_or_get_dt_node -l "CLICpix2_control$index" -n "CLICpix2_control" -u [regsub -all {^0x} $baseAddress {}] -p [gen_peripheral_nodes $drv_handle]]

    hsi::utils::add_new_dts_param $ControlNode "compatible" "xlnx,CLICpix2-control-1.0" stringlist
    #hsi::utils::add_new_dts_param $ControlNode "interrupt_parent" [hsi::utils::get_interrupt_parent $drv_handle "control_irpt"] reference
    set intr_info [get_intr_id $drv_handle "control_irpt"]
    if { [llength $intr_info] && ![string match -nocase $intr_info "-1"] } {
	hsi::utils::add_new_dts_param $ControlNode "interrupts" $intr_info intlist
    } else {
	dtg_warning "ERROR: ${drv_handle}: receiver_irpt port is not connected"
    }

    set regs [split [get_property CONFIG.reg $drv_handle]]
    set regs_index [lsearch $regs $baseAddress]
    set reg_value [concat [lindex $regs $regs_index] [lindex $regs [expr $regs_index + 1]]]
    hsi::utils::add_new_dts_param $ControlNode "reg" $reg_value inthexlist    
    
}

proc add_spi {drv_handle index} {
    
    set i 0
    set ip_mem_handle [lindex [hsi::utils::get_ip_mem_ranges [get_cells -hier $drv_handle]] $i]
    while {[get_property ADDRESS_BLOCK $ip_mem_handle] != "Reg2"} {
	set ip_mem_handle [lindex [hsi::utils::get_ip_mem_ranges [get_cells -hier $drv_handle]] $i]
	incr i
    }
    if { [string_is_empty $ip_mem_handle] } {
	dtg_warning "ERROR: Can't find CLICpix2 SPI base address"
	return -1
    }
    set baseAddress [string tolower [get_property BASE_VALUE $ip_mem_handle]]
    set SPInode [add_or_get_dt_node -l "CLICpix2_spi$index" -n "CLICpix2_spi" -u [regsub -all {^0x} $baseAddress {}] -p [gen_peripheral_nodes $drv_handle]]

    hsi::utils::add_new_dts_param $SPInode "compatible" "xlnx,xps-spi-2.00.a" stringlist
    #hsi::utils::add_new_dts_param $SPInode "interrupt_parent" [hsi::utils::get_interrupt_parent $drv_handle "spi_irpt"] reference
    set intr_info [get_intr_id $drv_handle "spi_irpt"]
    if { [llength $intr_info] && ![string match -nocase $intr_info "-1"] } {
	hsi::utils::add_new_dts_param $SPInode "interrupts" $intr_info intlist
    } else {
	dtg_warning "ERROR: ${drv_handle}: spi_irpt port is not connected"
    }

    set regs [split [get_property CONFIG.reg $drv_handle]]
    set regs_index [lsearch $regs $baseAddress]
    set reg_value [concat [lindex $regs $regs_index] [lindex $regs [expr $regs_index + 1]]]
    hsi::utils::add_new_dts_param $SPInode "reg" $reg_value inthexlist

    #hardcoded generic values
    hsi::utils::add_new_dts_param $SPInode "bits-per-word" 16 int
    hsi::utils::add_new_dts_param $SPInode "fifo-size" 256 int
    hsi::utils::add_new_dts_param $SPInode "num-cs" 1 long
    hsi::utils::add_new_dts_param $SPInode "xlnx,num-ss-bits" 1 long
    hsi::utils::add_new_dts_param $SPInode "xlnx,spi-mode" 0 int
    #set alias, such it's alwasy spidev1.0
    set default_dts [get_property CONFIG.master_dts [get_os]]
    set system_root_node [add_or_get_dt_node -n "/" -d ${default_dts}]
    set alias_node [add_or_get_dt_node -n "aliases"  -d ${default_dts} -p ${system_root_node}]
    set spis [regexp -all "CONFIG.spi\d*" [list_property $alias_node] match]
    incr spis
    set spi_label [get_property "NODE_LABEL" $SPInode]
    hsi::utils::add_new_dts_param $alias_node spi$spis $spi_label aliasref
}
