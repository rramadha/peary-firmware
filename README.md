# Peary-firmware Caribou
 It is a firmware for [Peary DAQ](https://gitlab.cern.ch/Caribou/peary) which is supported by custom Linux image ([Yocto](https://www.yoctoproject.org/)) defined by [meta-caibou](https://gitlab.cern.ch/Caribou/meta-caribou).
 
# Build process
In the top directory call:
 ```
 $ ./run_batch.tcl 
 ```
The output bitfile can be found under `outputs/caribou-soc.bit`