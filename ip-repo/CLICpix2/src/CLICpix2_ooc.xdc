################################################################################

# This XDC is used only for OOC mode of synthesis, implementation
# This constraints file contains default clock frequencies to be used during
# out-of-context flows such as OOC Synthesis and Hierarchical Designs.
# This constraints file is not used in normal top-down synthesis (default flow
# of Vivado)
################################################################################
create_clock -name aclk -period 5 [get_ports aclk]
create_clock -name SI5345_CLK_OUT8_clk_p -period 10 [get_ports SI5345_CLK_OUT8_clk_p]
create_clock -name Transceiver_refClk_clk_p -period 3.125 [get_ports Transceiver_refClk_clk_p]

################################################################################