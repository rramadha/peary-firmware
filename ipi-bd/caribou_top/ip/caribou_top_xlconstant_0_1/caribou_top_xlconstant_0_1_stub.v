// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.3.1 (lin64) Build 2035080 Fri Oct 20 14:20:00 MDT 2017
// Date        : Thu Jan 11 12:17:49 2018
// Host        : adrian-laptop running 64-bit Ubuntu 17.10
// Command     : write_verilog -force -mode synth_stub
//               /home/afiergol/clic/Caribou/peary-firmware/ipi-bd/caribou_top/ip/caribou_top_xlconstant_0_1/caribou_top_xlconstant_0_1_stub.v
// Design      : caribou_top_xlconstant_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z045ffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "xlconstant_v1_1_3_xlconstant,Vivado 2017.3.1" *)
module caribou_top_xlconstant_0_1(dout)
/* synthesis syn_black_box black_box_pad_pin="dout[2:0]" */;
  output [2:0]dout;
endmodule
