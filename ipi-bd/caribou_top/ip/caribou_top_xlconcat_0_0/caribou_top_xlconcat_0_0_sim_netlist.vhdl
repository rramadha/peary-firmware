-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.3.1 (lin64) Build 2035080 Fri Oct 20 14:20:00 MDT 2017
-- Date        : Thu Jan 11 12:17:48 2018
-- Host        : adrian-laptop running 64-bit Ubuntu 17.10
-- Command     : write_vhdl -force -mode funcsim
--               /home/afiergol/clic/Caribou/peary-firmware/ipi-bd/caribou_top/ip/caribou_top_xlconcat_0_0/caribou_top_xlconcat_0_0_sim_netlist.vhdl
-- Design      : caribou_top_xlconcat_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z045ffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity caribou_top_xlconcat_0_0 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    In1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    dout : out STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of caribou_top_xlconcat_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of caribou_top_xlconcat_0_0 : entity is "caribou_top_xlconcat_0_0,xlconcat_v2_1_1_xlconcat,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of caribou_top_xlconcat_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of caribou_top_xlconcat_0_0 : entity is "xlconcat_v2_1_1_xlconcat,Vivado 2017.3.1";
end caribou_top_xlconcat_0_0;

architecture STRUCTURE of caribou_top_xlconcat_0_0 is
  signal \^in0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^in1\ : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  \^in0\(2 downto 0) <= In0(2 downto 0);
  \^in1\(0) <= In1(0);
  dout(3) <= \^in1\(0);
  dout(2 downto 0) <= \^in0\(2 downto 0);
end STRUCTURE;
