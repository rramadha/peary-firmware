-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.3.1 (lin64) Build 2035080 Fri Oct 20 14:20:00 MDT 2017
-- Date        : Thu Jan 11 12:17:55 2018
-- Host        : adrian-laptop running 64-bit Ubuntu 17.10
-- Command     : write_vhdl -force -mode funcsim
--               /home/afiergol/clic/Caribou/peary-firmware/ipi-bd/caribou_top/ip/caribou_top_Caribou_control_0_1/caribou_top_Caribou_control_0_1_sim_netlist.vhdl
-- Design      : caribou_top_Caribou_control_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z045ffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity caribou_top_Caribou_control_0_1_Caribou_control_AXI is
  port (
    wready : out STD_LOGIC;
    bresp : out STD_LOGIC_VECTOR ( 0 to 0 );
    rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bvalid : out STD_LOGIC;
    rvalid : out STD_LOGIC;
    rresp : out STD_LOGIC_VECTOR ( 0 to 0 );
    awvalid : in STD_LOGIC;
    wvalid : in STD_LOGIC;
    araddr : in STD_LOGIC_VECTOR ( 0 to 0 );
    arvalid : in STD_LOGIC;
    rready : in STD_LOGIC;
    aclk : in STD_LOGIC;
    aresetN : in STD_LOGIC;
    bready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of caribou_top_Caribou_control_0_1_Caribou_control_AXI : entity is "Caribou_control_AXI";
end caribou_top_Caribou_control_0_1_Caribou_control_AXI;

architecture STRUCTURE of caribou_top_Caribou_control_0_1_Caribou_control_AXI is
  signal \rdata[31]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \registers[0][q]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal stateR : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \stateR[0]_i_1_n_0\ : STD_LOGIC;
  signal \stateR[1]_i_1_n_0\ : STD_LOGIC;
  signal \stateR[1]_i_2_n_0\ : STD_LOGIC;
  signal stateW : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \stateW[1]_i_1_n_0\ : STD_LOGIC;
  signal NLW_firmware_version_CFGCLK_UNCONNECTED : STD_LOGIC;
  signal NLW_firmware_version_DATAVALID_UNCONNECTED : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \__3/i_\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \bresp[0]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of bvalid_INST_0 : label is "soft_lutpair3";
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of firmware_version : label is "PRIMITIVE";
  attribute SOFT_HLUTNM of \rdata[0]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \rdata[10]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \rdata[11]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \rdata[12]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \rdata[13]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \rdata[14]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \rdata[15]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \rdata[16]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \rdata[17]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \rdata[18]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \rdata[19]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \rdata[1]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \rdata[20]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \rdata[21]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \rdata[22]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \rdata[23]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \rdata[24]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \rdata[25]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \rdata[26]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \rdata[27]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \rdata[28]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \rdata[29]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \rdata[2]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \rdata[30]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \rdata[31]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \rdata[3]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \rdata[4]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \rdata[5]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \rdata[6]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \rdata[7]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \rdata[8]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \rdata[9]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of rvalid_INST_0 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \stateR[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \stateR[1]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \stateW[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of wready_INST_0 : label is "soft_lutpair1";
begin
\__3/i_\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5540"
    )
        port map (
      I0 => stateR(0),
      I1 => araddr(0),
      I2 => arvalid,
      I3 => stateR(1),
      O => rresp(0)
    );
\bresp[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => stateW(1),
      I1 => wvalid,
      I2 => awvalid,
      O => bresp(0)
    );
bvalid_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => wvalid,
      I1 => awvalid,
      I2 => stateW(1),
      O => bvalid
    );
firmware_version: unisim.vcomponents.USR_ACCESSE2
     port map (
      CFGCLK => NLW_firmware_version_CFGCLK_UNCONNECTED,
      DATA(31 downto 0) => \registers[0][q]\(31 downto 0),
      DATAVALID => NLW_firmware_version_DATAVALID_UNCONNECTED
    );
\rdata[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(0),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(0)
    );
\rdata[10]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(10),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(10)
    );
\rdata[11]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(11),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(11)
    );
\rdata[12]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(12),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(12)
    );
\rdata[13]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(13),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(13)
    );
\rdata[14]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(14),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(14)
    );
\rdata[15]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(15),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(15)
    );
\rdata[16]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(16),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(16)
    );
\rdata[17]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(17),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(17)
    );
\rdata[18]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(18),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(18)
    );
\rdata[19]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(19),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(19)
    );
\rdata[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(1),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(1)
    );
\rdata[20]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(20),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(20)
    );
\rdata[21]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(21),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(21)
    );
\rdata[22]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(22),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(22)
    );
\rdata[23]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(23),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(23)
    );
\rdata[24]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(24),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(24)
    );
\rdata[25]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(25),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(25)
    );
\rdata[26]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(26),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(26)
    );
\rdata[27]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(27),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(27)
    );
\rdata[28]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(28),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(28)
    );
\rdata[29]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(29),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(29)
    );
\rdata[2]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(2),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(2)
    );
\rdata[30]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(30),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(30)
    );
\rdata[31]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(31),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(31)
    );
\rdata[31]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5504"
    )
        port map (
      I0 => stateR(1),
      I1 => arvalid,
      I2 => araddr(0),
      I3 => stateR(0),
      O => \rdata[31]_INST_0_i_1_n_0\
    );
\rdata[3]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(3),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(3)
    );
\rdata[4]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(4),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(4)
    );
\rdata[5]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(5),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(5)
    );
\rdata[6]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(6),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(6)
    );
\rdata[7]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(7),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(7)
    );
\rdata[8]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(8),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(8)
    );
\rdata[9]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \registers[0][q]\(9),
      I1 => \rdata[31]_INST_0_i_1_n_0\,
      O => rdata(9)
    );
rvalid_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3E"
    )
        port map (
      I0 => arvalid,
      I1 => stateR(1),
      I2 => stateR(0),
      O => rvalid
    );
\stateR[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C0C0CDCC"
    )
        port map (
      I0 => araddr(0),
      I1 => stateR(0),
      I2 => stateR(1),
      I3 => arvalid,
      I4 => rready,
      O => \stateR[0]_i_1_n_0\
    );
\stateR[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C0C0F2F0"
    )
        port map (
      I0 => araddr(0),
      I1 => stateR(0),
      I2 => stateR(1),
      I3 => arvalid,
      I4 => rready,
      O => \stateR[1]_i_1_n_0\
    );
\stateR[1]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => aresetN,
      O => \stateR[1]_i_2_n_0\
    );
\stateR_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => aclk,
      CE => '1',
      CLR => \stateR[1]_i_2_n_0\,
      D => \stateR[0]_i_1_n_0\,
      Q => stateR(0)
    );
\stateR_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => aclk,
      CE => '1',
      CLR => \stateR[1]_i_2_n_0\,
      D => \stateR[1]_i_1_n_0\,
      Q => stateR(1)
    );
\stateW[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5540"
    )
        port map (
      I0 => bready,
      I1 => wvalid,
      I2 => awvalid,
      I3 => stateW(1),
      O => \stateW[1]_i_1_n_0\
    );
\stateW_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => aclk,
      CE => '1',
      CLR => \stateR[1]_i_2_n_0\,
      D => \stateW[1]_i_1_n_0\,
      Q => stateW(1)
    );
wready_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => awvalid,
      I1 => wvalid,
      I2 => stateW(1),
      O => wready
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity caribou_top_Caribou_control_0_1_Caribou_control is
  port (
    awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    awvalid : in STD_LOGIC;
    awready : out STD_LOGIC;
    wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    wvalid : in STD_LOGIC;
    wready : out STD_LOGIC;
    bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    bvalid : out STD_LOGIC;
    bready : in STD_LOGIC;
    araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    arvalid : in STD_LOGIC;
    arready : out STD_LOGIC;
    rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    rvalid : out STD_LOGIC;
    rready : in STD_LOGIC;
    aclk : in STD_LOGIC;
    aresetN : in STD_LOGIC
  );
  attribute AXI_ADDR_WIDTH : integer;
  attribute AXI_ADDR_WIDTH of caribou_top_Caribou_control_0_1_Caribou_control : entity is 3;
  attribute AXI_DATA_WIDTH : integer;
  attribute AXI_DATA_WIDTH of caribou_top_Caribou_control_0_1_Caribou_control : entity is 32;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of caribou_top_Caribou_control_0_1_Caribou_control : entity is "Caribou_control";
end caribou_top_Caribou_control_0_1_Caribou_control;

architecture STRUCTURE of caribou_top_Caribou_control_0_1_Caribou_control is
  signal \<const1>\ : STD_LOGIC;
  signal \^bresp\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^rresp\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \^wready\ : STD_LOGIC;
begin
  arready <= \<const1>\;
  awready <= \^wready\;
  bresp(1) <= \^bresp\(0);
  bresp(0) <= \^bresp\(0);
  rresp(1) <= \^rresp\(1);
  rresp(0) <= \^rresp\(1);
  wready <= \^wready\;
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
axi_fsm: entity work.caribou_top_Caribou_control_0_1_Caribou_control_AXI
     port map (
      aclk => aclk,
      araddr(0) => araddr(2),
      aresetN => aresetN,
      arvalid => arvalid,
      awvalid => awvalid,
      bready => bready,
      bresp(0) => \^bresp\(0),
      bvalid => bvalid,
      rdata(31 downto 0) => rdata(31 downto 0),
      rready => rready,
      rresp(0) => \^rresp\(1),
      rvalid => rvalid,
      wready => \^wready\,
      wvalid => wvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity caribou_top_Caribou_control_0_1 is
  port (
    awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    awvalid : in STD_LOGIC;
    awready : out STD_LOGIC;
    wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    wvalid : in STD_LOGIC;
    wready : out STD_LOGIC;
    bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    bvalid : out STD_LOGIC;
    bready : in STD_LOGIC;
    araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    arvalid : in STD_LOGIC;
    arready : out STD_LOGIC;
    rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    rvalid : out STD_LOGIC;
    rready : in STD_LOGIC;
    aclk : in STD_LOGIC;
    aresetN : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of caribou_top_Caribou_control_0_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of caribou_top_Caribou_control_0_1 : entity is "caribou_top_Caribou_control_0_1,Caribou_control,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of caribou_top_Caribou_control_0_1 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of caribou_top_Caribou_control_0_1 : entity is "Caribou_control,Vivado 2017.3.1";
end caribou_top_Caribou_control_0_1;

architecture STRUCTURE of caribou_top_Caribou_control_0_1 is
  attribute AXI_ADDR_WIDTH : integer;
  attribute AXI_ADDR_WIDTH of inst : label is 3;
  attribute AXI_DATA_WIDTH : integer;
  attribute AXI_DATA_WIDTH of inst : label is 32;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of aclk : signal is "xilinx.com:signal:clock:1.0 aclk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of aclk : signal is "XIL_INTERFACENAME aclk, ASSOCIATED_BUSIF axi, ASSOCIATED_RESET aresetN, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN caribou_top_processing_system7_0_0_FCLK_CLK0";
  attribute X_INTERFACE_INFO of aresetN : signal is "xilinx.com:signal:reset:1.0 aresetN RST";
  attribute X_INTERFACE_PARAMETER of aresetN : signal is "XIL_INTERFACENAME aresetN, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of arready : signal is "xilinx.com:interface:aximm:1.0 axi ARREADY";
  attribute X_INTERFACE_INFO of arvalid : signal is "xilinx.com:interface:aximm:1.0 axi ARVALID";
  attribute X_INTERFACE_INFO of awready : signal is "xilinx.com:interface:aximm:1.0 axi AWREADY";
  attribute X_INTERFACE_INFO of awvalid : signal is "xilinx.com:interface:aximm:1.0 axi AWVALID";
  attribute X_INTERFACE_INFO of bready : signal is "xilinx.com:interface:aximm:1.0 axi BREADY";
  attribute X_INTERFACE_INFO of bvalid : signal is "xilinx.com:interface:aximm:1.0 axi BVALID";
  attribute X_INTERFACE_INFO of rready : signal is "xilinx.com:interface:aximm:1.0 axi RREADY";
  attribute X_INTERFACE_PARAMETER of rready : signal is "XIL_INTERFACENAME axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 200000000, ID_WIDTH 0, ADDR_WIDTH 3, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN caribou_top_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  attribute X_INTERFACE_INFO of rvalid : signal is "xilinx.com:interface:aximm:1.0 axi RVALID";
  attribute X_INTERFACE_INFO of wready : signal is "xilinx.com:interface:aximm:1.0 axi WREADY";
  attribute X_INTERFACE_INFO of wvalid : signal is "xilinx.com:interface:aximm:1.0 axi WVALID";
  attribute X_INTERFACE_INFO of araddr : signal is "xilinx.com:interface:aximm:1.0 axi ARADDR";
  attribute X_INTERFACE_INFO of arprot : signal is "xilinx.com:interface:aximm:1.0 axi ARPROT";
  attribute X_INTERFACE_INFO of awaddr : signal is "xilinx.com:interface:aximm:1.0 axi AWADDR";
  attribute X_INTERFACE_INFO of awprot : signal is "xilinx.com:interface:aximm:1.0 axi AWPROT";
  attribute X_INTERFACE_INFO of bresp : signal is "xilinx.com:interface:aximm:1.0 axi BRESP";
  attribute X_INTERFACE_INFO of rdata : signal is "xilinx.com:interface:aximm:1.0 axi RDATA";
  attribute X_INTERFACE_INFO of rresp : signal is "xilinx.com:interface:aximm:1.0 axi RRESP";
  attribute X_INTERFACE_INFO of wdata : signal is "xilinx.com:interface:aximm:1.0 axi WDATA";
  attribute X_INTERFACE_INFO of wstrb : signal is "xilinx.com:interface:aximm:1.0 axi WSTRB";
begin
inst: entity work.caribou_top_Caribou_control_0_1_Caribou_control
     port map (
      aclk => aclk,
      araddr(2 downto 0) => araddr(2 downto 0),
      aresetN => aresetN,
      arprot(2 downto 0) => arprot(2 downto 0),
      arready => arready,
      arvalid => arvalid,
      awaddr(2 downto 0) => awaddr(2 downto 0),
      awprot(2 downto 0) => awprot(2 downto 0),
      awready => awready,
      awvalid => awvalid,
      bready => bready,
      bresp(1 downto 0) => bresp(1 downto 0),
      bvalid => bvalid,
      rdata(31 downto 0) => rdata(31 downto 0),
      rready => rready,
      rresp(1 downto 0) => rresp(1 downto 0),
      rvalid => rvalid,
      wdata(31 downto 0) => wdata(31 downto 0),
      wready => wready,
      wstrb(3 downto 0) => wstrb(3 downto 0),
      wvalid => wvalid
    );
end STRUCTURE;
