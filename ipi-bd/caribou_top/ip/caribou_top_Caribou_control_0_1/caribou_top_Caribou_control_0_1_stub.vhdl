-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.3.1 (lin64) Build 2035080 Fri Oct 20 14:20:00 MDT 2017
-- Date        : Thu Jan 11 12:17:55 2018
-- Host        : adrian-laptop running 64-bit Ubuntu 17.10
-- Command     : write_vhdl -force -mode synth_stub
--               /home/afiergol/clic/Caribou/peary-firmware/ipi-bd/caribou_top/ip/caribou_top_Caribou_control_0_1/caribou_top_Caribou_control_0_1_stub.vhdl
-- Design      : caribou_top_Caribou_control_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z045ffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity caribou_top_Caribou_control_0_1 is
  Port ( 
    awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    awvalid : in STD_LOGIC;
    awready : out STD_LOGIC;
    wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    wvalid : in STD_LOGIC;
    wready : out STD_LOGIC;
    bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    bvalid : out STD_LOGIC;
    bready : in STD_LOGIC;
    araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    arvalid : in STD_LOGIC;
    arready : out STD_LOGIC;
    rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    rvalid : out STD_LOGIC;
    rready : in STD_LOGIC;
    aclk : in STD_LOGIC;
    aresetN : in STD_LOGIC
  );

end caribou_top_Caribou_control_0_1;

architecture stub of caribou_top_Caribou_control_0_1 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "awaddr[2:0],awprot[2:0],awvalid,awready,wdata[31:0],wstrb[3:0],wvalid,wready,bresp[1:0],bvalid,bready,araddr[2:0],arprot[2:0],arvalid,arready,rdata[31:0],rresp[1:0],rvalid,rready,aclk,aresetN";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "Caribou_control,Vivado 2017.3.1";
begin
end;
