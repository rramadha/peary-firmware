create_clock -name axi_lite_clk -period 5 [get_ports aclk] 
create_clock -name ext_spi_clk -period 5 [get_ports ext_spi_clk]
create_clock -name clicpix2_slow_clock -period 10 [get_ports CLICpix2_slow_clock] -waveform {1 6}
        
set_clock_groups -asynchronous -group {axi_lite_clk} -group {ext_spi_clk clicpix2_slow_clock CLICpix2_slow_clock_miso}
	
