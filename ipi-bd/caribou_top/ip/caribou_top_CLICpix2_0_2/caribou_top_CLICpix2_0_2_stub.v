// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.3.1 (lin64) Build 2035080 Fri Oct 20 14:20:00 MDT 2017
// Date        : Tue Jan 23 12:48:43 2018
// Host        : adrian-laptop running 64-bit Ubuntu 17.10
// Command     : write_verilog -force -mode synth_stub
//               /home/afiergol/clic/Caribou/peary-firmware/ipi-bd/caribou_top/ip/caribou_top_CLICpix2_0_2/caribou_top_CLICpix2_0_2_stub.v
// Design      : caribou_top_CLICpix2_0_2
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z045ffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "CLICpix2,Vivado 2017.3.1" *)
module caribou_top_CLICpix2_0_2(AXI_araddr, AXI_arburst, AXI_arcache, 
  AXI_arlen, AXI_arlock, AXI_arprot, AXI_arqos, AXI_arready, AXI_arregion, AXI_arsize, 
  AXI_arvalid, AXI_awaddr, AXI_awburst, AXI_awcache, AXI_awlen, AXI_awlock, AXI_awprot, 
  AXI_awqos, AXI_awready, AXI_awregion, AXI_awsize, AXI_awvalid, AXI_bready, AXI_bresp, 
  AXI_bvalid, AXI_rdata, AXI_rlast, AXI_rready, AXI_rresp, AXI_rvalid, AXI_wdata, AXI_wlast, 
  AXI_wready, AXI_wstrb, AXI_wvalid, C3PD_reset_n, C3PD_reset_p, CLICpix2_miso_n, 
  CLICpix2_miso_p, CLICpix2_mosi_n, CLICpix2_mosi_p, CLICpix2_pwr_pulse_n, 
  CLICpix2_pwr_pulse_p, CLICpix2_reset_n, CLICpix2_reset_p, CLICpix2_shutter_n, 
  CLICpix2_shutter_p, CLICpix2_ss_n, CLICpix2_ss_p, CLICpix2_tp_sw_n, CLICpix2_tp_sw_p, 
  SI5345_CLK_OUT8_clk_n, SI5345_CLK_OUT8_clk_p, TLU_busy_n, TLU_busy_p, TLU_reset_n, 
  TLU_reset_p, TLU_trigger_n, TLU_trigger_p, Transceiver_RX_n, Transceiver_RX_p, 
  Transceiver_refClk_clk_n, Transceiver_refClk_clk_p, aclk, aresetN, control_irpt, 
  pll_locked, receiver_irpt, spi_irpt)
/* synthesis syn_black_box black_box_pad_pin="AXI_araddr[30:0],AXI_arburst[1:0],AXI_arcache[3:0],AXI_arlen[7:0],AXI_arlock[0:0],AXI_arprot[2:0],AXI_arqos[3:0],AXI_arready,AXI_arregion[3:0],AXI_arsize[2:0],AXI_arvalid,AXI_awaddr[30:0],AXI_awburst[1:0],AXI_awcache[3:0],AXI_awlen[7:0],AXI_awlock[0:0],AXI_awprot[2:0],AXI_awqos[3:0],AXI_awready,AXI_awregion[3:0],AXI_awsize[2:0],AXI_awvalid,AXI_bready,AXI_bresp[1:0],AXI_bvalid,AXI_rdata[31:0],AXI_rlast,AXI_rready,AXI_rresp[1:0],AXI_rvalid,AXI_wdata[31:0],AXI_wlast,AXI_wready,AXI_wstrb[3:0],AXI_wvalid,C3PD_reset_n,C3PD_reset_p,CLICpix2_miso_n,CLICpix2_miso_p,CLICpix2_mosi_n,CLICpix2_mosi_p,CLICpix2_pwr_pulse_n,CLICpix2_pwr_pulse_p,CLICpix2_reset_n,CLICpix2_reset_p,CLICpix2_shutter_n,CLICpix2_shutter_p,CLICpix2_ss_n,CLICpix2_ss_p,CLICpix2_tp_sw_n,CLICpix2_tp_sw_p,SI5345_CLK_OUT8_clk_n,SI5345_CLK_OUT8_clk_p,TLU_busy_n,TLU_busy_p,TLU_reset_n,TLU_reset_p,TLU_trigger_n,TLU_trigger_p,Transceiver_RX_n,Transceiver_RX_p,Transceiver_refClk_clk_n,Transceiver_refClk_clk_p,aclk,aresetN,control_irpt,pll_locked,receiver_irpt,spi_irpt" */;
  input [30:0]AXI_araddr;
  input [1:0]AXI_arburst;
  input [3:0]AXI_arcache;
  input [7:0]AXI_arlen;
  input [0:0]AXI_arlock;
  input [2:0]AXI_arprot;
  input [3:0]AXI_arqos;
  output AXI_arready;
  input [3:0]AXI_arregion;
  input [2:0]AXI_arsize;
  input AXI_arvalid;
  input [30:0]AXI_awaddr;
  input [1:0]AXI_awburst;
  input [3:0]AXI_awcache;
  input [7:0]AXI_awlen;
  input [0:0]AXI_awlock;
  input [2:0]AXI_awprot;
  input [3:0]AXI_awqos;
  output AXI_awready;
  input [3:0]AXI_awregion;
  input [2:0]AXI_awsize;
  input AXI_awvalid;
  input AXI_bready;
  output [1:0]AXI_bresp;
  output AXI_bvalid;
  output [31:0]AXI_rdata;
  output AXI_rlast;
  input AXI_rready;
  output [1:0]AXI_rresp;
  output AXI_rvalid;
  input [31:0]AXI_wdata;
  input AXI_wlast;
  output AXI_wready;
  input [3:0]AXI_wstrb;
  input AXI_wvalid;
  output C3PD_reset_n;
  output C3PD_reset_p;
  input CLICpix2_miso_n;
  input CLICpix2_miso_p;
  output CLICpix2_mosi_n;
  output CLICpix2_mosi_p;
  output CLICpix2_pwr_pulse_n;
  output CLICpix2_pwr_pulse_p;
  output CLICpix2_reset_n;
  output CLICpix2_reset_p;
  output CLICpix2_shutter_n;
  output CLICpix2_shutter_p;
  output CLICpix2_ss_n;
  output CLICpix2_ss_p;
  output CLICpix2_tp_sw_n;
  output CLICpix2_tp_sw_p;
  input SI5345_CLK_OUT8_clk_n;
  input SI5345_CLK_OUT8_clk_p;
  output TLU_busy_n;
  output TLU_busy_p;
  input TLU_reset_n;
  input TLU_reset_p;
  input TLU_trigger_n;
  input TLU_trigger_p;
  input Transceiver_RX_n;
  input Transceiver_RX_p;
  input Transceiver_refClk_clk_n;
  input Transceiver_refClk_clk_p;
  input aclk;
  input aresetN;
  output control_irpt;
  output pll_locked;
  output receiver_irpt;
  output spi_irpt;
endmodule
