//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_receiver_ClockInterface.sv
// Description     : Clock interface.
// Author          : Adrian Fiergolski
// Created On      : Mon Apr 24 14:46:10 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`include "CLICpix2_receiver_Package.sv"

interface CLICpix2_receiver_ClockInterface;

   import CLICpix2_receiver_Package::CLOCK_DOMAIN;
  
   CLOCK_DOMAIN cdr;  //recovered clock from the CLICpix2 Transceiver (40 MHz)
   assign cdr.rst = ~ cdr.rstN;
   assign cdr.locked = cdr.rst;
   
   CLOCK_DOMAIN sysClk; //reference clk for reset FSM inside the Transceiver (64 MHz)

   CLOCK_DOMAIN axiClk; //clock used by the external AXI4-Lite interface
   
   //Reference clock for transceiver (320 MHz)
   struct {
      logic p;
      logic n;} refClk;

endinterface // CLICpix2_receiver_ClockInterface

  
