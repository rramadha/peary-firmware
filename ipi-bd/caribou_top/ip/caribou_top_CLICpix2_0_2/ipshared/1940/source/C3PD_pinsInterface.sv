//                              -*- Mode: Verilog -*-
// Filename        : C3PD_pinsInterface.sv
// Description     : Interface handling C3PD pins.
// Author          : Adrian Fiergolski
// Created On      : Mon May  8 17:26:55 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

interface C3PD_pinsInterface;
   logic reset;
endinterface // C3PD_pinsInterface
  
